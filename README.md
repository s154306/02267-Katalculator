## Maven Template Project

![Related image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSH5LYHgnH8kBgtnVzjjmKjmwFYHzqsgNiyX0CrxiWkvRUdR9Py&s)

This repo contains a template project using the Maven package manger, as well as having example tests for both JUnit and Cucumber

### Quickstart

#### Install

```
mvn install
```

#### Compile project

```
mvn package
```

#### Run your tests

```
mvn test
```

#### Clean repo from compiled files

```
mvn clean
```

### Structure

The repo follows the structure specified in [Apache Maven's documentation](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html), for quick reference see the following

```
├── README.md
├── pom.xml <-- this holds dependencies and build descriptions and stuff
└── src
    ├── main <-- this is where your code will live
    │   ├── java
    │   │   └── Main.java <-- main class and file
    │   └── resources
    └── test <-- tests live here
        ├── java
        │   ├── ExampleScenarioSteps.java <-- this file 
        │   ├── JUnitExampleTest.java <-- this is just a JUnit test
        │   └── RunCucumberTest.java <-- this file runs all the Cucumber tests
        └── resources
            └── example_scenario.feature <-- this is a "scenario" descriping a file, see Cucumber
```

### Dependencies

- Java8 (yeah I know...)
- Maven

### Todo

- Add linter
- Add commit hook configuration

