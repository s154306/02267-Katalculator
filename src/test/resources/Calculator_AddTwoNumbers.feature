Feature: Does the string equal the right amount?
    Everybody wants to know it. Nobody knows it.

    Scenario: Two numbers get added
    Given A string containing two numbers
    When I ask what the sum of the two numbers is
    Then The two numbers get added