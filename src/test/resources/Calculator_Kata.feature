Feature: Is the string empty?
  Everybody wants to know if the string is empty

  Scenario: The string is empty
    Given the string is empty
    When I ask whether the string is empty
    Then The result is 0
