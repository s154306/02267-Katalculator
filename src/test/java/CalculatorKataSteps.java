import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

public class CalculatorKataSteps {
    Main main = new Main();

    String string;
    int result;

    @Given("the string is empty")
    public void the_string_is_empty() {
        string = "";
    }

    @When("I ask whether the string is empty")
    public void i_ask_whether_the_string_is_empty() throws Exception {
        result = main.Add(string);
    }

    @Then("The result is {int}")
    public void the_result_is(int actualAnswer) {
        assertEquals(result, actualAnswer);

    }

    @Given("A string containing two numbers")
    public void a_string_containing_two_numbers() {
        string = "6,9";
    }

    @When("I ask what the sum of the two numbers is")
    public void i_ask_what_the_sum_of_the_two_numbers_is() throws Exception {
        result = main.Add(string);
    }

    @Then("The two numbers get added")
    public void the_two_numbers_get_added() {
        assertEquals(15, result);
    }

}
