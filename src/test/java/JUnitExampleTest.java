// this file is just a simple JUnit test(s)

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class JUnitExampleTest {
    Main m;
    @BeforeClass
    public static void setUpClass() throws Exception {
        // code executed before the first test method
    }

    @Before
    public void setUp() throws Exception {
        m = new Main();
    }

    @Test
    public void testEmptyString() throws Exception {
        int result = m.Add("");

        assertEquals(result, 0);
    }

    @Test
    public void testOneNumber() throws Exception {
        int result = m.Add("3");

        assertEquals(result, 3);
    }

    @Test
    public void testTwoNumbers() throws Exception {
        int result = m.Add("3,2");

        assertEquals(result, 5);
    }

    @Test
    public void testThreeNumbers() throws Exception {
        Integer result = m.Add("3,2,1");

        assertEquals(new Integer(6), result);
    }

    @Test
    public void testTenNumbers() throws Exception {
        Integer result = m.Add("3,2,1,5,6,4,3,5,7,8");

        assertEquals(new Integer(44), result);
    }

    @Test(expected = Exception.class)
    public void testNegativeNumber() throws Exception {
        Integer result = m.Add("3,2,1,-5,6,-4,3,5,7,8");

    }

    @Test
    public void testTenNumbersWithNewLine() throws Exception {
        Integer result = m.Add("3,2,1,5,6,4,3,5\n7,8");

        assertEquals(new Integer(44), result);
    }

    @Test
    public void testTenNumbersCustomDelimiter() throws Exception {
        Integer result = m.Add("//;\n3;2;1;5;6;4;3;5\n7,8");

        assertEquals(new Integer(44), result);
    }

    @Test(expected = NumberFormatException.class)
    public void testTenNumbersOneLetter() throws Exception {
        Integer result = m.Add("3,2,1,5,6,4,3,5,7,8,h");
    }

    @Test(expected = NumberFormatException.class)
    public void testLetter() throws Exception {
        Integer result = m.Add("d");
    }

    @Test(expected = NumberFormatException.class)
    public void testNotComma() throws Exception {
        Integer result = m.Add("5´6");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        // code executed after the last test method
    }
}
