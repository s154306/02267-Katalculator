// this file contains the main part of the program

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        // put your main code here
        System.out.println("Hello, World!");
    }

    public Integer Add(String numbers) throws Exception{
        int numberLength = numbers.length();

        if (numbers.isEmpty()) {
            return 0;
        } else if (numberLength == 1) {
            return Integer.parseInt(numbers);
        } else {
            try {
                return splitAndAdd(numbers);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                throw e;
            }
        }
    }

    private int splitAndAdd(String numbers) throws Exception{
        String[] parts = split(numbers);
        int totalNumber = 0;

        ArrayList<Integer> negativeNumbers = new ArrayList();
        for (String number : parts) {
            int currentNumber = Integer.parseInt(number);
            if (yeet(currentNumber)) {
                negativeNumbers.add(currentNumber);
            }
            totalNumber += currentNumber;
        }
        if (!negativeNumbers.isEmpty()) {
            throw new Exception(negativeNumbers.toString());
        }

        return totalNumber;
    }

    private String[] split(String numbers) {
        String regex = "[,\\n]";
        Optional<String> delimiter = checkDelimiter(numbers);
        if (delimiter.isPresent()) {
            numbers = numbers.substring(4);
            regex += "|" + delimiter.get();
        }
        return numbers.split(regex);
    }

    private boolean yeet(int input) {
        return input < 0;
    }

    private Optional<String> checkDelimiter(String input) {
        if (input.substring(0, 2).equals("//")){
            String result = input.substring(2, 3);
            return Optional.of(result);
        } else {
            return Optional.empty();
        }
    }
}
